FROM registry.gitlab.com/haybot/docker-images:run

WORKDIR /home/haybot

COPY ./target/armv7-unknown-linux-gnueabihf/release/haybot .
ADD ./resources/ ./resources/

RUN git clone git@gitlab.com:haybot/DB.git

CMD ["./haybot"]
