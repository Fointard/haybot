use proc_macros::slash_cmd;
use serenity::utils::Colour;

use crate::{core::player::Player, utils::embed::make_embed_author};

#[slash_cmd]
fn start(ctx: Ctx, int: AppCmdInt, mbds: Vec<Embed>) {
	let lock = Player::new(int.user.id)?;
	let p = lock.read().await;

	int.create_interaction_response(&ctx, |response| {
		response
			.kind(InteractionResponseType::ChannelMessageWithSource)
			.interaction_response_data(|m| {
				m.add_embeds(mbds);

				m.embed(|e| {
					e.author(|e| make_embed_author(e, &int.user, &p))
						.title("Votre périple commence...")
						.color(Colour::DARK_GREEN)
				})
			})
	})
	.await
	.context("Error while creating interaction response")
	.map_err(CmdError::from)
}
