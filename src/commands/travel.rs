use anyhow::{anyhow, Result};
use proc_macros::slash_cmd;
use serenity::utils::Colour;

use crate::{
	core::{
		localization::Localization,
		path::{Path, Transport},
		player::Player,
	},
	utils::{constants::*, embed::make_embed_author},
};

#[slash_cmd]
fn travel(ctx: Ctx, int: AppCmdInt, mbds: Vec<Embed>) {
	let lock = Player::load(int.user.id)?;
	let p = lock.read().await;

	if p.is_travelling() {
		return Err(CmdError::BeforeResponse(anyhow!(
			"Vous avez déjà entrepris un voyage"
		)));
	}

	let location = match &p.localization {
		Localization::Moving(_, t, _) => t,
		Localization::Still(l) => l,
	}
	.get_data()?;

	let destinations = location
		.paths
		.iter()
		.filter(|path| p.wealth >= path.cost())
		.map(|p| Ok((p, ron::to_string(p)?)))
		.collect::<Result<Vec<_>>>()
		.context("Error while serializing path")?;

	int.create_interaction_response(&ctx, |response| {
		response
			.kind(InteractionResponseType::ChannelMessageWithSource)
			.interaction_response_data(|m| {
				m.add_embeds(mbds);

				m.embed(|e| {
					e.author(|e| make_embed_author(e, &int.user, &p))
						.title("Voyager")
						.field(
							"Localisation actuelle :earth_africa:",
							p.localization.to_string(),
							false,
						)
						.color(Colour::ROHRKATZE_BLUE)
				});

				drop(p);

				m.components(|c| {
					c.create_action_row(|row| {
						row.create_select_menu(|menu| {
							menu.placeholder("Choisir une destination")
								.custom_id("travel_destination_choice")
								.options(|ops| {
									destinations.iter().for_each(|(p, v)| {
										ops.create_option(|op| {
											op.label(p).value(v).emoji(match p.transport {
												Transport::Pedestrian => '🚶',
												Transport::Maritime(_) => '⛵',
											})
										});
									});
									ops
								})
						})
					})
				})
			})
	})
	.await
	.context("Error while creating interaction response")?;

	let mut int_response = int
		.get_interaction_response(&ctx)
		.await
		.context("Error while retrieving interaction response")?;

	match int_response
		.await_component_interaction(&ctx)
		.author_id(int.user.id)
		.timeout(INTERACTION_TIMEOUT)
		.await
	{
		None => Err(CmdError::AfterResponse(anyhow!(
			"Aucune destination sélectionnée, voyage annulé."
		))),

		Some(i) => {
			let path: Path = ron::from_str(i.data.values.get(0).ok_or_else(|| {
				CmdError::AfterResponse(anyhow!("Error while retrieving selected destination"))
			})?)
			.context("Error while deserializing destination")
			.map_err(CmdError::after_response)?;

			let arrival_time = path.arrival_time().map_err(CmdError::after_response)?;
			let cost = path.cost();

			let mut p = lock.write().await;

			if cost > p.wealth {
				return Err(CmdError::AfterResponse(anyhow!(
					"Vous ne disposez pas des ressources pour financer ce voyage."
				)));
			} else if p.is_travelling() {
				return Err(CmdError::AfterResponse(anyhow!(
					"Vous avez déjà entrepris un voyage"
				)));
			}

			p.wealth = p
				.wealth
				.checked_sub(cost)
				.ok_or_else(|| CmdError::AfterResponse(anyhow!("Error while paying the cost")))?;

			let location = match &p.localization {
				Localization::Moving(_, t, _) => t,
				Localization::Still(l) => l,
			}
			.clone();

			p.localization = Localization::Moving(location, path.destination, arrival_time);
			let mut message = p.localization.to_string();

			p.save();
			drop(p);

			if cost > 0 {
				message += &format!("\nIl vous en coûtera {} pièces.", cost);
			}

			int_response
				.edit(&ctx, |m| {
					m.components(|c| c).add_embed(|e| {
						e.field("Nouvelle destination", message, false)
							.color(Colour::DARK_GREEN)
					})
				})
				.await
				.context("Error while editing interaction response")
				.map_err(CmdError::after_response)
		}
	}
}
