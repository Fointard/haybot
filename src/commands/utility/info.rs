use proc_macros::slash_cmd;
use serenity::prelude::Mentionable;

use crate::core::bot::Bot;

#[slash_cmd]
fn info(ctx: Ctx, int: AppCmdInt, mbds: Vec<Embed>) {
	let bot_data = Bot::get_data()?;
	int.create_interaction_response(&ctx, |response| {
		response
			.kind(InteractionResponseType::ChannelMessageWithSource)
			.interaction_response_data(|m| {
				m.add_embeds(mbds);

				m.embed(|e| {
					e.set_author(bot_data.embed_author.clone())
						.title("Informations")
						.field("Version", env!("CARGO_PKG_VERSION"), true)
						.field("Commit", env!("CI_COMMIT_SHORT_SHA"), true)
						.field("Auteur", bot_data.owner.mention(), true)
						.field("Source", env!("CI_PROJECT_URL"), false)
				})
			})
	})
	.await
	.context("Error while creating interaction response")
	.map_err(CmdError::from)
}
