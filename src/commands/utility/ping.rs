use proc_macros::slash_cmd;
use serenity::model::Timestamp;

use crate::core::bot::Bot;

#[slash_cmd]
fn ping(ctx: Ctx, int: AppCmdInt, mbds: Vec<Embed>) {
	let bot_data = Bot::get_data()?;
	int.create_interaction_response(&ctx, |response| {
		response
			.kind(InteractionResponseType::ChannelMessageWithSource)
			.interaction_response_data(|m| {
				m.add_embeds(mbds);

				m.embed(|e| {
					e.set_author(bot_data.embed_author.clone())
						.title("Ping")
						.field(
							"Pong :ping_pong:",
							(Timestamp::now().time() - int.id.created_at().time())
								.whole_milliseconds()
								.to_string() + "ms",
							false,
						)
				})
			})
	})
	.await
	.context("Error while creating interaction response")
	.map_err(CmdError::from)
}
