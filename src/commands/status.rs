use proc_macros::slash_cmd;

use crate::{core::player::Player, utils::embed::make_embed_author};

#[slash_cmd]
fn status(ctx: Ctx, int: AppCmdInt, mbds: Vec<Embed>) {
	let lock = Player::load(int.user.id)?;
	let p = lock.read().await;

	int.create_interaction_response(&ctx, |response| {
		response
			.kind(InteractionResponseType::ChannelMessageWithSource)
			.interaction_response_data(|m| {
				m.add_embeds(mbds);

				m.embed(|e| {
					e.author(|e| make_embed_author(e, &int.user, &p))
						.title("Status")
						.field(
							"Santé :heart:",
							format!("`{}/{}`", p.health, p.max_health),
							true,
						)
						.field(
							"Expérience :arrow_double_up:",
							format!("`{}/{}`", p.experience, p.exp_to_next_lvl()),
							true,
						)
						.field("Pièces :coin:", format!("`{}`", &p.wealth), true)
						.field("Localisation :earth_africa:", &p.localization, false)
				})
			})
	})
	.await
	.context("Error while creating interaction response")
	.map_err(CmdError::from)
}
