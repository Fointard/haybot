mod commands;
mod core;
mod server;
mod utils;

use anyhow::Result;

use crate::{
	core::{bot::Bot, location::Location, player::Player},
	server::{discord_client::create_client, signal_handler::SignalHandler},
};

#[tokio::main]
async fn main() -> Result<()> {
	git!("pull")?;

	let mut client = create_client().await?;

	// initialize global data
	Bot::init(&client.cache_and_http.http).await?;
	Location::init()?;
	Player::init()?;
	SignalHandler::init(client.shard_manager.clone())?;

	// start a single shard
	match client.start().await {
		Err(why) => eprintln!("Error while running the client: {}", why),
		Ok(_) => println!("Client gracefully disconnected"),
	}

	Player::save_to_repo().await
}
