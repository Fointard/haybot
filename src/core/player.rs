use std::{
	fs::File,
	ops::Deref,
	path::Path,
	sync::{
		atomic::{AtomicU8, Ordering},
		Arc,
	},
};

use anyhow::{bail, Context, Result};
use dashmap::{
	mapref::entry::Entry::{Occupied, Vacant},
	DashMap,
};
use rmp_serde::{decode, encode};
use serde::{Deserialize, Serialize};
use serenity::{
	model::{id::UserId, Timestamp},
	prelude::RwLock,
};
use tokio::sync::OnceCell;

use crate::{
	core::{localization::Localization, location::Location},
	utils::constants::*,
};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default, deny_unknown_fields)]
pub struct Player {
	pub level: u32,
	pub experience: u32,
	pub health: u32,
	pub max_health: u32,
	pub wealth: u32,
	pub localization: Localization,
	#[serde(skip, default)]
	serialize: bool,
}

type LockedPlayer = Arc<RwLock<Player>>;

// Initialized in main() on startup
static PLAYERS: OnceCell<DashMap<UserId, LockedPlayer>> = OnceCell::const_new();

impl Default for Player {
	fn default() -> Self {
		Self {
			level: 1,
			experience: 0,
			health: START_HEALTH,
			max_health: START_HEALTH,
			wealth: START_WEALTH,
			localization: Localization::Still(Location::default()),
			serialize: true,
		}
	}
}

impl Player {
	pub fn earn_xp(&mut self, mut earned_xp: u32) -> Option<u32> {
		let mut lvl_up = false;
		let mut lvl_up_xp = self.exp_to_next_lvl() - self.experience;

		if earned_xp >= lvl_up_xp {
			lvl_up = true;
			self.experience = 0;
		}

		while earned_xp >= lvl_up_xp {
			earned_xp -= lvl_up_xp;
			self.level += 1;
			lvl_up_xp = self.exp_to_next_lvl();
		}

		self.experience += earned_xp;

		lvl_up.then_some(self.level)
	}

	pub fn exp_to_next_lvl(&self) -> u32 {
		EXP_CST_COEF + ((self.level * EXP_PROP_COEF) as f32).powf(EXP_EXP_COEF) as u32
	}

	pub fn has_arrived(&self) -> bool {
		matches!(self.localization, Localization::Moving(..) if !self.is_travelling())
	}

	pub fn is_travelling(&self) -> bool {
		if let Localization::Moving(_, _, a) = self.localization {
			return (*a.deref() - *Timestamp::now().deref()).is_positive();
		}
		false
	}

	pub fn new<U>(id: U) -> Result<LockedPlayer>
	where
		U: Into<UserId>,
	{
		let id = id.into();
		match Self::get_ram_players()?.entry(id) {
			Vacant(v) if Self::load_from_disk(id).is_err() => {
				Ok(v.insert(Default::default()).clone())
			}
			_ => bail!("Ce joueur existe déjà"),
		}
	}

	pub fn update_loc(&mut self) {
		if let Localization::Moving(_, t, _) = &self.localization {
			self.localization = Localization::Still(t.clone());
		}
	}
}

// Memory related methods
impl Player {
	#[inline]
	fn get_ram_players() -> Result<&'static DashMap<UserId, LockedPlayer>> {
		PLAYERS
			.get()
			.context("Error while getting LOCATIONS content")
	}

	pub fn init() -> Result<()> {
		PLAYERS
			.set(DashMap::new())
			.context("Error while initializing PLAYERS")
	}

	pub fn load<U>(id: U) -> Result<LockedPlayer>
	where
		U: Into<UserId>,
	{
		let id = id.into();
		match Self::get_ram_players()?.entry(id) {
			Occupied(o) => Ok(o.get().clone()),
			Vacant(v) => Self::load_from_disk(id).map(|lock| v.insert(lock).clone()),
		}
		.context("Ce joueur n'existe pas")
	}

	fn load_from_disk(id: UserId) -> Result<LockedPlayer> {
		Ok(Arc::new(RwLock::new(
			decode::from_read::<_, Player>(
				File::open(
					Path::new(env!("DB_PATH"))
						.join("players")
						.join(Path::new(&id.to_string())),
				)
				.context("Aucune sauvegarde pour ce joueur.")?,
			)
			.context("Error while deserializing Player")?,
		)))
	}

	pub fn save(&mut self) {
		self.serialize = true;

		static SAVE_COUNT: AtomicU8 = AtomicU8::new(0);
		match SAVE_COUNT.fetch_update(Ordering::SeqCst, Ordering::SeqCst, |c| {
			Some((c + 1) % SAVE_COUNT_TRIGGER)
		}) {
			Err(_) => eprintln!("Error while updating save count"),
			Ok(count) => {
				if count == (SAVE_COUNT_TRIGGER - 1) {
					tokio::task::spawn(async move {
						if let Err(why) = Self::save_to_repo().await {
							eprintln!("Error while saving to repo: {}", why);
						}
					});
				}
			}
		}
	}

	async fn save_all_to_disk() -> Result<()> {
		for p in Self::get_ram_players()? {
			let (id, lock) = p.pair();
			Self::save_to_disk(id, lock).await?;
		}
		Ok(())
	}

	async fn save_to_disk(id: &UserId, lock: &LockedPlayer) -> Result<()> {
		let mut p = lock.write().await;
		if p.serialize {
			p.serialize = false;
			let p = p.downgrade();
			encode::write(
				&mut File::create(
					Path::new(env!("DB_PATH"))
						.join("players")
						.join(Path::new(&id.to_string())),
				)
				.context("Error while saving to disk")?,
				p.deref(),
			)
			.context("Error while serializing to disk")?;
		}
		Ok(())
	}

	pub async fn save_to_repo() -> Result<()> {
		Self::save_all_to_disk().await?;
		#[cfg(feature = "enable_repo_save")]
		{
			println!("Saving to repo");
			crate::git!("gamesave")?;
		}
		#[cfg(not(feature = "enable_repo_save"))]
		println!("Repo save is disabled");

		Ok(())
	}
}
