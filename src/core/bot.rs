use anyhow::{anyhow, Context, Result};
use serenity::{builder::CreateEmbedAuthor, http::Http, model::user::User};
use tokio::sync::OnceCell;

#[derive(Debug)]
pub struct Bot {
	pub owner: User,
	pub embed_author: CreateEmbedAuthor,
}

// Initialized in main() on startup
static BOT: OnceCell<Bot> = OnceCell::const_new();

impl Bot {
	pub fn get_data() -> Result<&'static Self> {
		BOT.get()
			.ok_or_else(|| anyhow!("Error while getting BOT content"))
	}

	pub async fn init(http: &Http) -> Result<()> {
		let owner = http
			.get_current_application_info()
			.await
			.context("Error during HTTP request")?
			.owner;

		let embed_author = CreateEmbedAuthor::default()
			.name(env!("CARGO_PKG_NAME"))
			.icon_url(
				http.get_current_user()
					.await
					.context("Error during HTTP request")?
					.face(),
			)
			.url(env!("CI_PAGES_URL"))
			.to_owned();

		BOT.set(Self {
			owner,
			embed_author,
		})
		.context("Error while initializing BOT")
	}
}
