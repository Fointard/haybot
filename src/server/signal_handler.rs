use std::sync::Arc;

use anyhow::{Context, Result};
use serenity::{client::bridge::gateway::ShardManager, prelude::Mutex};
use tokio::signal::unix::{signal, SignalKind};

pub struct SignalHandler;

impl SignalHandler {
	pub fn init(sm: Arc<Mutex<ShardManager>>) -> Result<()> {
		let mut sig_stream =
			signal(SignalKind::terminate()).context("Error while creating signal stream")?;

		tokio::spawn(async move {
			sig_stream.recv().await;
			sm.lock().await.shutdown_all().await;
		});

		Ok(())
	}
}
