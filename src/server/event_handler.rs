use serenity::{
	async_trait,
	builder::CreateEmbed,
	client::{Context, EventHandler},
	model::{
		application::interaction::application_command::ApplicationCommandInteraction,
		prelude::{
			command::Command,
			interaction::{Interaction, InteractionResponseType},
			Ready,
		},
	},
};

use crate::{
	commands::{
		start::*,
		status::*,
		travel::*,
		utility::{info::*, ping::*},
	},
	core::player::Player,
	utils::{constants::*, embed::make_error_embed, error::CmdError},
};

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
	async fn ready(&self, ctx: Context, _: Ready) {
		if let Err(why) = Command::set_global_application_commands(&ctx, |cmds| {
			cmds.create_application_command(|cmd| {
				cmd.name("démarrer")
					.description("Démarre la partie en créant un personnage")
			})
			.create_application_command(|cmd| {
				cmd.name("info").description("Affiche des infos sur le bot")
			})
			.create_application_command(|cmd| {
				cmd.name("ping")
					.description("Envoie un ping, reçois un pong !")
			})
			.create_application_command(|cmd| {
				cmd.name("status")
					.description("Affiche des infos sur un joueur")
			})
			.create_application_command(|cmd| {
				cmd.name("voyager")
					.description("Débute un voyage vers la destination choisie")
			})
		})
		.await
		{
			eprintln!("Error while creating slash commands: {}", why);
			std::process::exit(1);
		}
	}

	async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
		if let Interaction::ApplicationCommand(int) = interaction {
			let mbds = Self::before_interaction(&int).await;
			Self::handle_interaction(ctx, int, mbds).await;
			Self::after_interaction();
		}
	}
}

impl Handler {
	fn after_interaction() {}

	async fn before_interaction(int: &ApplicationCommandInteraction) -> Vec<CreateEmbed> {
		let mut mbds = vec![];

		if let Ok(lock) = Player::load(int.user.id) {
			let p = lock.read().await;

			let mut new_lvl = None;

			if p.has_arrived() {
				drop(p);
				let mut p = lock.write().await;

				p.update_loc();
				new_lvl = p.earn_xp(TRIP_XP_GAIN);
				p.save();

				let p = p.downgrade();

				let mut mbd = CreateEmbed::default();
				mbd.title("Vous êtes arrivé à destination")
					.field("Expérience acquise", format!("`{}`", TRIP_XP_GAIN), true)
					.field("Nouvelle localisation", &p.localization, false);
				mbds.push(mbd);
			}

			if let Some(lvl) = new_lvl {
				let mut mbd = CreateEmbed::default();
				mbd.title(format!("Vous êtes désormais niveau {lvl}"));
				mbds.push(mbd);
			}
		}

		mbds
	}

	async fn handle_interaction(
		ctx: Context,
		int: ApplicationCommandInteraction,
		mbds: Vec<CreateEmbed>,
	) {
		if let Err(cmd_err) = (match int.data.name.as_str() {
			"démarrer" => start,
			"info" => info,
			"ping" => ping,
			"status" => status,
			"voyager" => travel,
			_ => unimplemented!(),
		})(&ctx, &int, mbds)
		.await
		{
			match cmd_err {
				CmdError::AfterResponse(err) => {
					if let Err(why) = int
						.edit_original_interaction_response(&ctx, |m| {
							m.components(|c| c).add_embed(make_error_embed(err))
						})
						.await
					{
						eprintln!("Error while editing original interaction response: {}", why);
					}
				}

				CmdError::BeforeResponse(err) => {
					if let Err(why) = int
						.create_interaction_response(&ctx, |response| {
							response
								.kind(InteractionResponseType::ChannelMessageWithSource)
								.interaction_response_data(|m| {
									m.add_embed(
										make_error_embed(err)
											.author(|a| {
												a.name(&int.user.name).icon_url(&int.user.face())
											})
											.to_owned(),
									)
									.ephemeral(true)
								})
						})
						.await
					{
						eprintln!("Error while sending error response: {}", why);
					}
				}
			}
		}
	}
}
