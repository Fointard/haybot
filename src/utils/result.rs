use anyhow::Result;

use crate::utils::error::CmdError;

pub type CmdResult = Result<(), CmdError>;
