use core::time::Duration;

// timings
pub const INTERACTION_TIMEOUT: Duration = Duration::from_secs(15);

pub const ONE_HOUR: f64 = 3600f64;
pub const ONE_MINUTE: f64 = 60f64;

// memory
pub const SAVE_COUNT_TRIGGER: u8 = 10;

// player
pub const EXP_CST_COEF: u32 = 61;
pub const EXP_EXP_COEF: f32 = 1.3;
pub const EXP_PROP_COEF: u32 = 17;

pub const START_HEALTH: u32 = 50;
pub const START_WEALTH: u32 = 100;

pub const TRIP_XP_GAIN: u32 = 50;
