#[macro_export]
macro_rules! git {
	($args:literal) => {{
		let cmd = anyhow::Context::context(
			std::process::Command::new("git")
				.args($args.split_whitespace())
				.current_dir(env!("DB_PATH"))
				.output(),
			"Error while executing command",
		)?;
		cmd.status.success().then_some(()).ok_or(anyhow::anyhow!(
			"Error during `git {}` ({}:{}):\n\n\t{}\n",
			$args,
			file!(),
			line!(),
			std::string::String::from_utf8_lossy(&cmd.stderr)
				.trim()
				.replace("\n", "\n\t"),
		))
	}};
}
