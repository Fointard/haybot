use anyhow::Error;
use serenity::{
	builder::{CreateEmbed, CreateEmbedAuthor},
	model::user::User,
	utils::Colour,
};

use crate::core::player::Player;

pub fn make_embed_author<'a>(
	e: &'a mut CreateEmbedAuthor,
	user: &User,
	p: &Player,
) -> &'a mut CreateEmbedAuthor {
	e.name(format!("{} - Niveau {}", user.name, p.level))
		.icon_url(user.face())
}

pub fn make_error_embed(err: Error) -> CreateEmbed {
	CreateEmbed::default()
		.color(Colour::RED)
		.field("Erreur :shrug:", &err, false)
		.footer(|f| {
			f.text(
				err.chain()
					.skip(1)
					.map(|i| format!("{}\n", i))
					.collect::<String>(),
			)
		})
		.to_owned()
}
