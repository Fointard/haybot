use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, ItemFn};

#[proc_macro_attribute]
pub fn slash_cmd(_: TokenStream, item: TokenStream) -> TokenStream {
	let item = parse_macro_input!(item as ItemFn);
	let fn_name_ident = item.sig.ident;
	let fn_block = item.block;

	quote! {
		use anyhow::Context;
		use serenity::model::prelude::interaction::InteractionResponseType;
		use crate::utils::error::CmdError;

		pub fn #fn_name_ident<'a>(
			ctx: &'a serenity::client::Context,
			int: &'a serenity::model::prelude::interaction::application_command::ApplicationCommandInteraction,
			mbds: Vec<serenity::builder::CreateEmbed>,
		) -> serenity::futures::future::BoxFuture<'a, crate::utils::result::CmdResult> {
			serenity::FutureExt::boxed(async move {
				#fn_block
			})
		}
	}
	.into()
}
